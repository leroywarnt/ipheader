"""Handles the ineractive input for users"""
from header import Header
import socket
import time
import random
START_TIME = time.time()

class Input(Header):
    """Inherits Header and provides interactive prompts for user input"""
    def __init__(self):
        self.version = 0
        self.src = ""
        self.dst = ""
        self.out_fmt = ""
        self.initial_request()
        super().__init__(self.version, self.src, self.dst)
        super().__init__(self.version, socket.inet_ntop(Header.addressfamily(self.version),self.src), socket.inet_ntop(Header.addressfamily(self.version),self.dst), **self.ask_for_more())
        self.package() #pack with defaults
        self.compute_checksum() #compute for defaults
        self.package() #with with computed
        self.compute_checksum() #compute for packed
        self.ask_for_output_fmt()

    def initial_request(self):
        """Retrieves version, source IP and destination IP from input. If nothing is entered, defaults will be used."""
        while True:
            try:
                self.version = int(input("What version do you want to feast your eyes upon, kind Sir? 6/[4]:  ") or "4")
            except:
                print(Input.random_response("failed"))
            if self.version == 4:
                self.src = input("What Source Address would you like to have, your Lordship? [10.0.0.1]:  ") or "10.0.0.1"
                self.dst = input("What Destination Address would like to have, me Lady? [10.0.0.250]:  ") or "10.0.0.250"
                break
            elif self.version == 6:
                self.src = input("What Source Address would you like to have, your Lordship? [beef:8008:5::10]:  ") or "beef:8008:5::10"
                self.dst = input("What Destination Address would like to have, me Lady? [beef:8008:5::250]:  ") or "beef:8008:5::250"
                break
            else:
                print(" IP version {} not ratified yet. Please contact your trusted IEEE representative.".format(self.version))

    def ask_for_more(self):
        """Allows for the optional addition of other header field values. If nothing is entered, defaults will be used."""
        valid_dict_v4 = {"ttl" : self.ttl, "tos" : self.tos, "protocol" : self.protocol, "identification" : self.identification,
                         "flags" : self.flags, "offset" : self.offset, "options" : self.options}
        valid_dict_v6 = {"hop_limit" : self.ttl, "traffic_class" : self.tos, "next_header" : self.protocol, "flowlabel" :self.flowlabel}
        valid_dict = valid_dict_v4 if self.version == 4 else valid_dict_v6
        add_this = ""
        while True:
            add_this = input("Would you like to add anything? yes/[no]:  ") or "no"
            if add_this == "no":
                return valid_dict
            elif add_this == "yes":
                break
            else:
                print(Input.random_response("failed"))
        while True:
            add_this = input("What is it that your heart desires? [help]:  ") or "help"
            if add_this == "help":
                print(" Use: arg = value\n Optional arguments that can be set\n\t {}\n Enter 'quit' to save and exit.".format(valid_dict))
            elif add_this == "quit":
                print(Input.random_response("continue"))
                break
            else:
                add_this = "".join(add_this.split()).replace(" ", "") #remove all whitespace and space characters
                if add_this.split("=")[0] in valid_dict.keys():
                    valid_dict[add_this.split("=")[0]] = add_this.split("=")[1]
                else:
                    print(Input.random_response("failed"))
        # because the names need to be different :/
        if self.version == 6:
            valid_dict["ttl"] = valid_dict.pop("hop_limit")
            valid_dict["tos"] = valid_dict.pop("traffic_class")
            valid_dict["protocol"] = valid_dict.pop("next_header")
        return valid_dict

    def ask_for_output_fmt(self):
        """Prompts for output format, decimal or binary"""
        while True:
            output_fmt = input("How would you like it presented? bin/[dec]: ") or "dec"
            output_fmt = "".join(output_fmt.split()).replace(" ", "")
            if output_fmt == "dec" or output_fmt == "bin":
                break
            else:
                print(Input.random_response("failed"))
        self.out_fmt = output_fmt
        return self.out_fmt

    @staticmethod
    def random_response(condition):
        """Outputs a success or failure response for the user"""
        responses_continue = [" Excellent choice, Sir!", " Superb!", " Marvelous!", " Splendid",
                              " Couldn't have done it better myself.", " Good shot, Sir!",
                              " That's a winner!", " A wise move.", " Keen and bold. Success guaranteed!",
                              " Winner, winner, chicken dinner!", " Bravo, me Lord!", " That is pristine!"]
        responses_failed = [" Yes, but...", " Consider the consequences!", " Maybe not so wise.",
                            " A valuable input, but not what I was looking for.", " Try again please.",
                            " Fool me once, shame on me.", " Fool me twice, shame on me.",
                            " It's easy to fool the eyes, but it's hard to fool the heart."]
        response = responses_continue if condition == "continue" else responses_failed
        return response[random.randint(0, len(response)-1)]

if __name__ == "__main__":
    t = Input()
    print(t.output_to_string(t.out_fmt))

    print("\n--- {} seconds ---".format(time.time() - START_TIME))
