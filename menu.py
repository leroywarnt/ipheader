from header import Header
from input import Input

def menu():
    while True:
        to_do = input("\nWhat do you want to do? [help]: ") or "help"
        if to_do == "help":
            print("You can: \n build - Build your own header")
            print(" paste - Paste in header data")
            print(" exit - Leave me, like you do all your friends")
            print(" checksum - Test the checksum (requires header in string format)")
            print(" yeet - yeets")
        elif to_do == "build":
            t = Input()
            print("Behold! Your creation: \n")
            print(t.output_to_string(t.out_fmt))
        elif to_do == "paste":
            notation = input("What format will the string have? bin/[dec]: ") or "dec"
            if notation == "dec":
                notation = "bin"
            else:
                notation = "dec"
            new = Header.input_from_string(input("\n Put your string here: "))
            new.package()
            new.compute_checksum()
            new.package()
            print("You did this: \n")
            print(new.output_to_string(notation))
            print("Checksum is: {}".format(new.checksum) if new.version == 4 else "\n Nice!")
        elif to_do == "exit":
            break
        elif to_do == "checksum":
            newchecksum = Header.input_from_string(input("\n Put your string here: "))
            newchecksum.package()
            newchecksum.compute_checksum()
            print("Checksum is: {}".format(newchecksum.checksum))
            if newchecksum.checksum == 0:
                print("Yeet! It's 0.")
            else:
                print("Not very good at security!")
        elif to_do == "yeet":
            print("\nYEET!")
        else:
            print("\nCould you repeat that? I didn't quite get it.\n")


if __name__ == "__main__":
    print("Let's do this...\n")
    menu()
    print("\nAt least I have chicken!")
