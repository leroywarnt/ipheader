"""Provides access to the (IPv4/6) Header class and its functions"""
import socket
#import time
import bitstruct
#START_TIME = time.time()

class Header:
    """Represents the IPvX header.

    OVERLAPPING HEADER FIELDS:

        IPv4            <=>   IPv6
        -----------------------------------
        Type of Service <=>   Traffic Class
        Protocol        <=>   Next Header
        Time To Live    <=>   Hop Limit
        -----------------------------------"""
    # bit structure for the ipv4/6 header, see referrence picture
    fmt_list_ipv4 = ["u4", "u4", "u8", "u16", "u16", "u3", "u13", "u8", "u8", "u16", "r32", "r32", "u24", "u8"]
    fmt_list_ipv6 = ["u4", "u8", "u20", "u16", "u8", "u8", "r128", "r128"]

    def __init__(self, version, src, dst, **kwargs):
        # defaults
        self.tos = 24
        self.identification = self.flags = self.offset = self.protocol = self.options = self.checksum = self.padding = self.payload = 0
        self.flowlabel = 20
        self.ttl = 32
        # args
        self.version = version
        self.src = socket.inet_pton(Header.addressfamily(self.version), src)
        self.dst = socket.inet_pton(Header.addressfamily(self.version), dst)
        self.fmt_list = self.fmt_list_ipv4 if self.version == 4 else self.fmt_list_ipv6
        self.fmt = "".join(self.fmt_list)
        self.ihl = bitstruct.calcsize(self.fmt)/ 32
        self.length = self.ihl + self.payload if self.version == 4 else self.payload
        #create dict of all instance members and allow updating them via optional arguments
        self.__dict__.update()
        self.__dict__.update(**kwargs)

    def package(self):
        """Packs the header field variables (from dict values) into byte structure (in order!)"""
        #inet = lambda ip: int.from_bytes(socket.inet_pton(Header.addressfamily(self.version), ip), byteorder="big")
        if self.version == 4:
            self.packed = bitstruct.pack(self.fmt, self.__dict__["version"], self.__dict__["ihl"], self.__dict__["tos"],
                                         self.__dict__["length"], self.__dict__["identification"], self.__dict__["flags"],
                                         self.__dict__["offset"], self.__dict__["ttl"], self.__dict__["protocol"], self.__dict__["checksum"],
                                         self.__dict__["src"], self.__dict__["dst"], self.__dict__["options"], self.__dict__["padding"])
        elif self.version == 6:
            self.packed = bitstruct.pack(self.fmt, self.__dict__["version"], self.__dict__["tos"], self.__dict__["flowlabel"],
                                         self.__dict__["length"], self.__dict__["protocol"], self.__dict__["ttl"], self.__dict__["src"],
                                         self.__dict__["dst"])
        self.unpacked = bitstruct.unpack(self.fmt, self.packed)

    def compute_checksum(self):
        """Computes the (IPv4) header checksum.
        Definition: https://tools.ietf.org/html/rfc791
        Concept: https://en.wikipedia.org/wiki/IPv4_header_checksum#Calculation
        Derived from: https://stackoverflow.com/questions/3949726/calculate-ip-checksum-in-python"""
        checksum_result = 0
        # add up all 16 bit words
        for index in range(0, bitstruct.calcsize(self.fmt), 16):
            # get the first <index> bits, then get the last 16 (0xffff)
            checksum_result += (int.from_bytes(self.packed, byteorder="big") >> (index + 16)) & 0xffff
            # carry over if longer than 16 bit : (last 16 bit) + (anything over 16 bits)
            checksum_result = (checksum_result & 0xffff) + (checksum_result >> 16)
        # 1's complement
        self.checksum = ~checksum_result & 0xffff
        return self.checksum

    def output_to_string(self, output_format):
        """Generates the output for decimal or binary presentation.
            Arguments:
                    "dec"   <=>     decimal
                    "bin"   <=>     binary"""
        output = []
        out_fmt = output_format
        #format string in binary and prepend (fill) zeros based on width
        out_bin = lambda value, width: "{0:{fill}{width}b}".format((value + 2**width) % 2**width, fill="0", width=width)
        for index, value in enumerate(self.unpacked):
            #calcsize (bit length) of format list member at index of value ( -> header field width)
            field_width = bitstruct.calcsize(self.fmt_list[index])
            #output.append(out_bin(value, field_width)) if out_fmt == "bin" else output.append(str(value))
            if value.__class__ == int:
                output.append(out_bin(value, field_width)) if out_fmt == "bin" else output.append(str(value))
            elif value.__class__ == bytes:
                if out_fmt == "bin":
                    output.append(out_bin(int.from_bytes(value, byteorder="big"), field_width))
                else:
                    output.append(socket.inet_ntop(Header.addressfamily(self.version), value))
            #print(output)
        return " ".join(output) if out_fmt == "bin" else "-".join(output)

    def __repr__(self):
        """Redirects the output of print(<instance>) for ease of debugging"""
        return str(self.unpacked)

    @staticmethod
    def addressfamily(version):
        """Returns class representation of the version for the socket module"""
        return socket.AF_INET if version == 4 else socket.AF_INET6

    @classmethod
    def input_from_string(cls, input_string):
        """Creates a new Header object from the input string.
            Input string seperators allowed:
                        "-"     <=>     decimal input notation expected
                        " "     <=>     binary input notation expected"""
        to_string = lambda ver, intgr, size: socket.inet_ntop(Header.addressfamily(ver), bitstruct.pack("u%i" % size, intgr))
        if "-" in input_string:
            inpt = input_string.split("-")
            inpt = list(map(lambda x: str(x) if "." in x or ":" in x else int(x), inpt))
        elif " " in input_string:
            inpt = list(map(lambda x: int(x, 2), input_string.split()))
            #ipv4/6 address conversion for binary
            print(inpt)
            if inpt[0] == 4:
                for j in [10, 11]:
                    inpt[j] = to_string(inpt[0], inpt[j], 32)
            else:
                for j in [6, 7]:
                    inpt[j] = to_string(inpt[0], inpt[j], 128)
        else: raise Exception("You're doing it wrong! (*-*)")
        if inpt[0] == 4:
            new_header = cls(inpt[0], inpt[10], inpt[11], ihl=inpt[1], tos=inpt[2], length=inpt[3],
                             identification=inpt[4], flags=inpt[5], offset=inpt[6], ttl=inpt[7], protocol=inpt[8],
                             checksum=inpt[9], options=inpt[12], padding=inpt[13])
        else: new_header = cls(inpt[0], inpt[6], inpt[7], tos=inpt[1], flowlabel=inpt[2], length=inpt[3],
                               protocol=inpt[4], ttl=inpt[5])
        return new_header


if __name__ == "__main__":
    X = Header(4, "172.16.1.21", "172.16.1.250", ttl=31)
    X.package()
    print(X.output_to_string("dec"))
    #create k from bin/dec output of x
    K = Header.input_from_string(X.output_to_string("bin"))
    K.package()
    print("k: {}".format(K.output_to_string("dec")))
    #update y with dictionary
    UPD = {"ttl" : 207, "tos": 32}
    Y = Header(6, "beef:18e::8", "beef:18e::1", **UPD)
    Y.package()
    print("y = {}".format(Y))

    INPTSTR = "0100 0110 00011000 0000000000000110 0000000000000000 000 0000000000000 00100000 00000000 1000010011100110 00001010000000000000000000000001 00001010000000000000000011111010 000000000000000000000000 00000000"
    U = Header.input_from_string(INPTSTR)
    U.package()
    print(U.output_to_string("dec"))
    #z = Header.input_from_string(x.output_to_string("bin"))
    #z.package()
    #print(z)

    #print(hex(x.compute_checksum()))
    #x.compute_checksum()
    #x.package()
    #x.compute_checksum()
    #print(hex(x.checksum))

    #print(vars(x))
    #help(x)
    #print("\n--- {} seconds ---".format(time.time() - START_TIME))
